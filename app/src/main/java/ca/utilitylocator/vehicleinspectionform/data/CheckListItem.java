package ca.utilitylocator.vehicleinspectionform.data;


public class CheckListItem {

    public CheckListItem(String name,String state){
        this.name = name;
        this.status = state;
    }

    public String name;
    public String status;


}
