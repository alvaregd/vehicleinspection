package ca.utilitylocator.vehicleinspectionform.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;


import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.data.CheckListItem;


public class CheckListAdapter  extends ArrayAdapter<CheckListItem>{

    private Context context;

    public final static String YES = "YES";
    public final static String NO = "NO";
    public final static String UND = "Undefined";

    private RadioButton yes, no;

    public CheckListAdapter(Context context) {
        super(context, R.layout.entry_checklist);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /** inflate the view if null */
        if(convertView  == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_checklist,parent, false);
        }

        yes = (RadioButton)convertView.findViewById(R.id.rb_yes);
        no = (RadioButton)convertView.findViewById(R.id.rb_no);

        final CheckListItem item = getItem(position);

        if(item.status.equals(YES)){
            yes.setChecked(true);
            no.setChecked(false);
        }else if (item.status.equals(NO)){
            yes.setChecked(false);
            no.setChecked(true);
        }else if (item.status.equals(UND)){
            yes.setChecked(false);
            no.setChecked(false);
        }

        yes.setOnClickListener(getYesNewlistener(position));
        no.setOnClickListener(getNoNewlistener(position));

        ((TextView) convertView.findViewById(R.id.tv_name)).setText(getItem(position).name);
        return convertView;
    }

    public View.OnClickListener getYesNewlistener(final int position) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getItem(position).status = YES;
            }
        };
    }
    public View.OnClickListener getNoNewlistener(final int position) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getItem(position).status = NO;
            }
        };
    }



}
