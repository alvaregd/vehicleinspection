package ca.utilitylocator.vehicleinspectionform.data;


import ca.utilitylocator.vehicleinspectionform.adapters.CheckListAdapter;

public class Form {

    public String driver;
    public String plate;
    public String kit;
    public String departure;
    public String time;
    public String vehicleNumber;
    public String gps;
    public String destination;

    public int startKm = 0;
    public int endKm =0;
    public int totalKm = 0;

    public String headLights = CheckListAdapter.UND;
    public String tailLights =CheckListAdapter.UND;
    public String brakeLights =CheckListAdapter.UND;
    public String highBeam =CheckListAdapter.UND;
    public String signalLights =CheckListAdapter.UND;

    public String backupLights= CheckListAdapter.UND;
    public String parkingBrake = CheckListAdapter.UND;
    public String wheelTireTread =CheckListAdapter.UND;
    public String mirrorWindow = CheckListAdapter.UND;
    public String engineHoses=CheckListAdapter.UND;

    public String fluidLevels=CheckListAdapter.UND;
    public String belts=CheckListAdapter.UND;
    public String battery=CheckListAdapter.UND;
    public String exhaust=CheckListAdapter.UND;
    public String instrumentGauges=CheckListAdapter.UND;
    public String alarm=CheckListAdapter.UND;
    public String horn=CheckListAdapter.UND;
    public String heatDefrost=CheckListAdapter.UND;
    public String fireExtinguisher=CheckListAdapter.UND;
    public String frontierSafetyManual=CheckListAdapter.UND;
    public String MSDS=CheckListAdapter.UND;
    public String OHSA=CheckListAdapter.UND;
    public String firstAidKit=CheckListAdapter.UND;
    public String eyeWash=CheckListAdapter.UND;
    public String remarks;
    public String date;

    public boolean isSubmitted= false;
}
