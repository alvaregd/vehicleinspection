package ca.utilitylocator.vehicleinspectionform.fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

import ca.utilitylocator.vehicleinspectionform.interfaces.TimePickerCallBack;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    TimePickerCallBack listener;

    public void injectListener(TimePickerCallBack listener){
        this.listener = listener;
    }

    public static TimePickerFragment newInstance(TimePickerCallBack listener){
        final TimePickerFragment frag = new TimePickerFragment();
        frag.injectListener(listener);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        listener.setTime(hourOfDay,minute);
    }
}
