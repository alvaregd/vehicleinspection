package ca.utilitylocator.vehicleinspectionform.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.activity.FormActivty;
import ca.utilitylocator.vehicleinspectionform.data.Form;
import ca.utilitylocator.vehicleinspectionform.interfaces.TimePickerCallBack;

public class InfoTwoFragment extends Fragment{
    private final static String Tag  = InfoTwoFragment.class.getName();
    private final static boolean d_save = true;

    private View rootView;

    private static final String ARG_SECTION_NUMBER = "section_number";

    private Form form;

    private EditText time, vehicleNumber, gps, destination;

    private FormActivty.NextPage mListener;

    public static InfoTwoFragment newInstance(int sectionNumber, FormActivty.NextPage mListener) {
        InfoTwoFragment fragment = new InfoTwoFragment();
        fragment.mListener = mListener;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public InfoTwoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
             rootView = inflater.inflate(R.layout.fragment_trip_info_2, container, false);
        }

        form = ((FormActivty)getActivity()).getForm();

        time = (EditText)rootView.findViewById(R.id.et_time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragmentstart = TimePickerFragment.newInstance(new TimePickerCallBack() {
                    @Override
                    public void setTime(int hour, int minute) {
                        time.setText(hour +":"+minute);
                    }
                });

                newFragmentstart.show(getActivity().getSupportFragmentManager(), "timePickerStart");
            }
        });
        vehicleNumber = (EditText)rootView.findViewById(R.id.et_vehicle_number);
        gps = (EditText)rootView.findViewById(R.id.et_gps);
        destination = (EditText)rootView.findViewById(R.id.et_destination);

        time.setText(form.time);
        vehicleNumber.setText(form.vehicleNumber);
        gps.setText(form.gps);
        destination.setText(form.destination);

        ((Button)rootView.findViewById(R.id.b_next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFragment();
                mListener.onNextPageClicked();
            }
        });





        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        saveFragment();
    }

    public void saveFragment(){
        if(d_save) Log.e(Tag, "save One");
        form.time = time.getText().toString();
        form.vehicleNumber = vehicleNumber.getText().toString();
        form.gps = gps.getText().toString();
        form.destination = destination.getText().toString();
    }
}

