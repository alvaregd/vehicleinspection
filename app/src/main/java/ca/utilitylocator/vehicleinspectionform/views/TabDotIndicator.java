package ca.utilitylocator.vehicleinspectionform.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import ca.utilitylocator.vehicleinspectionform.R;

public class TabDotIndicator extends FrameLayout {

    private Context context;

    View tab1Guide, tab2Guide, tab3Guide,tab4Guide,tab5Guide;

    public TabDotIndicator(Context context) {
        super(context);
        initialize(context);
        this.context = context;
    }

    public TabDotIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        initialize(context);
    }

    public TabDotIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initialize(context);
    }

    public void initialize(Context context) {
        LayoutInflater lif = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        lif.inflate(R.layout.tab_dot_indicator, this);

        //initialize the buttons
        tab1Guide = (View) this.findViewById(R.id.tab_1_guide);
        tab2Guide = (View) this.findViewById(R.id.tab_2_guide);
        tab3Guide = (View) this.findViewById(R.id.tab_3_guide);
        tab4Guide = (View) this.findViewById(R.id.tab_4_guide);
        tab5Guide = (View) this.findViewById(R.id.tab_5_guide);

        tab1Guide.setBackgroundResource(R.drawable.tiny_circle);
        tab2Guide.setBackgroundResource(R.drawable.tiny_hollow_circle);
        tab3Guide.setBackgroundResource(R.drawable.tiny_hollow_circle);
        tab4Guide.setBackgroundResource(R.drawable.tiny_hollow_circle);
        tab5Guide.setBackgroundResource(R.drawable.tiny_hollow_circle);

    }

    public void setguide(int select) {
        Log.e("SetGuide", "Guide:" + select);

        tab1Guide.setBackgroundResource((select == 0) ?  R.drawable.tiny_circle :  R.drawable.tiny_hollow_circle);
        tab2Guide.setBackgroundResource((select == 1) ?  R.drawable.tiny_circle :  R.drawable.tiny_hollow_circle);
        tab3Guide.setBackgroundResource((select == 2) ?  R.drawable.tiny_circle :  R.drawable.tiny_hollow_circle);
        tab4Guide.setBackgroundResource((select == 3) ?  R.drawable.tiny_circle :  R.drawable.tiny_hollow_circle);
        tab5Guide.setBackgroundResource((select == 4) ?  R.drawable.tiny_circle :  R.drawable.tiny_hollow_circle);

    }

}
