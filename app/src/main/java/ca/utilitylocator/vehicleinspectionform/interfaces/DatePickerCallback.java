package ca.utilitylocator.vehicleinspectionform.interfaces;

/**
 * Created by alvaregd on 06/07/15.
 */
public interface DatePickerCallback {

    void setDate(int month, int day, int year);
}
