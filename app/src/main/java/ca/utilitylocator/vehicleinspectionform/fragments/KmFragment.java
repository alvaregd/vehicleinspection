package ca.utilitylocator.vehicleinspectionform.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.activity.FormActivty;
import ca.utilitylocator.vehicleinspectionform.data.Form;
import ca.utilitylocator.vehicleinspectionform.interfaces.DatePickerCallback;
import ca.utilitylocator.vehicleinspectionform.interfaces.TimePickerCallBack;


public class KmFragment extends Fragment {

    private final static String Tag = KmFragment.class.getName();
    private final static boolean d_save = true;

    private static final String ARG_SECTION_NUMBER = "section_number";

    View rootView;

    private Form form;

    EditText end, start, total,date,driver;

    public static KmFragment newInstance(int sectionNumber) {
        KmFragment fragment = new KmFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public KmFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView ==null){
            rootView = inflater.inflate(R.layout.fragment_final_page, container, false);
        }
        form = ((FormActivty)getActivity()).getForm();

        start = (EditText)rootView.findViewById(R.id.et_start);
        total = (EditText)rootView.findViewById(R.id.et_total);
        start.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int Stringstart, int before, int count) {

                if(!start.getText().toString().equals("") && !end.getText().toString().equals("") ){
                    total.setText(""+calcualteDiff(Integer.parseInt(s.toString()),Integer.parseInt(end.getText().toString())));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        end = (EditText)rootView.findViewById(R.id.et_end);
        end.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int estart, int before, int count) {
                if(!start.getText().toString().equals("") && !end.getText().toString().equals("") ){
                    total.setText(""+calcualteDiff(Integer.parseInt(start.getText().toString()),Integer.parseInt(s.toString())));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        date = (EditText) rootView.findViewById(R.id.et_date_sign);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* DialogFragment newFragmentstart = DatePickerFragment.newInstance(new DatePickerCallback() {
                    @Override
                    public void setDate(int month, int day, int year) {
                        date.setText((month + 1) +"/" + day + "/" + year);
                    }
                });
                newFragmentstart.show(getActivity().getSupportFragmentManager(), "timePickerStart");*/
            }
        });

        date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (date.getRight() - date.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
//                        Toast.makeText(getActivity(),"date picker",Toast.LENGTH_SHORT).show();
                        DialogFragment newFragmentstart = DatePickerFragment.newInstance(new DatePickerCallback() {
                            @Override
                            public void setDate(int month, int day, int year) {
                                date.setText((month + 1) +"/" + day + "/" + year);
                            }
                        });
                        newFragmentstart.show(getActivity().getSupportFragmentManager(), "timePickerStart");
                        return true;
                    }
                }
                return false;
            }
        });

        driver = (EditText) rootView.findViewById(R.id.et_driver_sign);

        if(form.startKm == 0){
            start.setText("");
        }else{
            start.setText(""+form.startKm);
        }

        if(form.endKm == 0){
            end.setText("");
        }else{
            end.setText(""+form.endKm);
        }

        if(form.totalKm == 0){
            total.setText("");
        }else{
            total.setText(""+form.totalKm);
        }

        driver.setText(form.driver);
        date.setText(form.date);


        rootView.findViewById(R.id.b_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        rootView.findViewById(R.id.b_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //SUBMIT

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"daveherbert@frontierlocating.com"});
                i.putExtra(Intent.EXTRA_SUBJECT, "Fleet Vehicle Inspection");
                i.putExtra(Intent.EXTRA_TEXT   ,

                        "Vehicle Inspection Form \n\n"
                                + "Driver: " + form.driver + "\n"
                                + "Time: " + form.time + "\n"
                                + "Licence Plate: " + form.plate + "\n"
                                + "Vehicle Number: " + form.vehicleNumber + "\n"
                                + "Kit: " + form.kit + "\n"
                                + "GPS: " + form.gps + "\n"
                                + "Departure Point: " + form.departure + "\n"
                                + "Destination: " + form.destination + "\n\n"

                                + "Start KM: " + form.startKm + "\n"
                                + "End Km: " + form.endKm + "\n"
                                + "Total: " + form.totalKm + "\n\n"
                                + "Check List: \n"
                                + "Head Lights: " + form.headLights + "\n"
                                + "Tail Lights: " + form.tailLights + "\n"
                                + "Brake Lights: " + form.brakeLights + "\n"
                                + "High Beam: " + form.highBeam + "\n"
                                + "Signal Lights: " + form.signalLights + "\n"
                                + "Backup Lights: " + form.backupLights + "\n"
                                + "Parking Break: " + form.parkingBrake + "\n"
                                + "Wheels - Tire & Tread: " + form.wheelTireTread + "\n"
                                + "Mirror & Window: " + form.mirrorWindow + "\n"
                                + "Engine Hoses: " + form.engineHoses + "\n"
                                + "Fluid Levels: " + form.fluidLevels + "\n"
                                + "Belts: " + form.belts + "\n"
                                + "Battery: " + form.battery + "\n"
                                + "Exhaust: " + form.exhaust + "\n"
                                + "Instrument Gauges: " + form.instrumentGauges + "\n"
                                + "Alarm: " + form.alarm + "\n"
                                + "Horn: " + form.horn + "\n"
                                + "Heat & Defrost: " + form.heatDefrost + "\n"
                                + "Fire Extinguisher: " + form.fireExtinguisher + "\n"
                                + "Frontier Safety Manual: " + form.frontierSafetyManual + "\n"
                                + "MSDS: " + form.MSDS + "\n"
                                + "OHSA: " + form.OHSA + "\n"
                                + "First Aid Kit: " + form.firstAidKit + "\n"
                                + "Eyewash " + form.eyeWash + "\n\n"

                                + "Remarks: " + form.remarks + "\n\n"
                                + "Date Signed: "  + form.date

                );

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }


    @Override
    public void onPause() {
        super.onPause();
        saveFragment();
    }

    public void saveFragment(){
        if(d_save) Log.e(Tag, "saving final");

        form.startKm = start.getText().toString().equals("") ? 0:Integer.parseInt(start.getText().toString());
        form.endKm = end.getText().toString().equals("") ?     0:Integer.parseInt(end.getText().toString());
        form.totalKm =total.getText().toString().equals("") ?  0: Integer.parseInt(total.getText().toString());
        form.date = date.getText().toString();
        form.driver = driver.getText().toString();
    }

    public int calcualteDiff(int a, int b){
        return b - a;
    }


}
