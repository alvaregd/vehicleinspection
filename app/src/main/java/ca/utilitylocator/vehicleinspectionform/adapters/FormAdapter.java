package ca.utilitylocator.vehicleinspectionform.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.Locale;

import ca.utilitylocator.vehicleinspectionform.activity.FormActivty;
import ca.utilitylocator.vehicleinspectionform.fragments.CheckListFragment;
import ca.utilitylocator.vehicleinspectionform.fragments.InfoOneFragment;
import ca.utilitylocator.vehicleinspectionform.fragments.InfoTwoFragment;
import ca.utilitylocator.vehicleinspectionform.fragments.KmFragment;
import ca.utilitylocator.vehicleinspectionform.fragments.RemarksFragment;

public class FormAdapter extends FragmentPagerAdapter {

    public final static int PAGE_INFO_1 = 0;
    public final static int PAGE_INFO_2 = 1;
    public final static int PAGE_CHECKLIST =2;
    public final static int PAGE_REMARK = 3;
    public final static int PAGE_KM = 4;

    public FormAdapter(FragmentManager fm, FormActivty.NextPage listener) {
        super(fm);
        this.listener = listener;
    }

    FormActivty.NextPage listener;

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position){
            case PAGE_INFO_1:    return InfoOneFragment.newInstance(position + 1, listener);
            case PAGE_INFO_2:    return InfoTwoFragment.newInstance(position + 1,listener);
            case PAGE_KM:        return KmFragment.newInstance(position +1);
            case PAGE_CHECKLIST: return CheckListFragment.newInstance(position + 1,listener);
            case PAGE_REMARK:    return RemarksFragment.newInstance(position +1,listener);
            default:             return InfoOneFragment.newInstance(position + 1,listener);
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        /*switch (position) {
            case 0:
                return getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return getString(R.string.title_section3).toUpperCase(l);
        }*/
        return null;
    }
}
