package ca.utilitylocator.vehicleinspectionform.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.activity.FormActivty;
import ca.utilitylocator.vehicleinspectionform.adapters.FormListAdapter;
import ca.utilitylocator.vehicleinspectionform.app.App;
import ca.utilitylocator.vehicleinspectionform.data.Form;
import ca.utilitylocator.vehicleinspectionform.data.FormCollection;

public class FormListFragment extends ListFragment {

    private final static String Tag = "RuleListFragment";
    private final static boolean d_onResume = true;
    private final static boolean d_listItemClick = false;

    //Cursor stuff
    private static final int ACTIVITY_CREATE = 0;
    private static final int ACTIVITY_EDIT = 1;
    private static final int DELETE_ID = Menu.FIRST + 1;

    // private Cursor cursor;

    FloatingActionButton fab;
    private FormListAdapter mAdapter;
    private View rootView;

    private FormCollection collection;

    public FormListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
        }

        fab = (FloatingActionButton)rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), FormActivty.class));
                Toast.makeText(getActivity(), "New Form", Toast.LENGTH_SHORT).show();
            }
        });

        mAdapter= new FormListAdapter(getActivity());

        setListAdapter(mAdapter);

        return rootView;
    }

    public void refreshData(){
        try{
            if(d_onResume)Log.e(Tag,"onResume()");
            mAdapter.clear();
            collection  = ((App)getActivity().getApplication()).getFormCollection();
            if(collection != null){
                if(collection.forms == null){
                    Log.e(Tag,"null forms ");
                }else{
                    Log.e(Tag,"Forms size: " + collection.forms.size());
                }

                if(mAdapter == null){
                    Log.e(Tag,"Adapter null");
                }

                for(int i = 0; i <collection.forms.size(); i++){
                    mAdapter.add(collection.forms.get(i).date + " " + collection.forms.get(i).time);
                }
            }
            mAdapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            Log.e(Tag,"NullPointer",e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fab.attachToListView(getListView());
        registerForContextMenu(getListView());
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case DELETE_ID:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                        .getMenuInfo();
                if(collection != null){
                    collection.forms.remove(info.position);
                }
                ((App)getActivity().getApplication()).saveFormCollection(collection);

                refreshData();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_ID, 0, R.string.action_delete);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Form form = collection.forms.get(position);

        if(d_listItemClick){
            Log.e(Tag,form.driver);
            Log.e(Tag,form.plate);
            Log.e(Tag,form.kit);
            Log.e(Tag,form.departure);
            Log.e(Tag,form.time);
            Log.e(Tag,form.vehicleNumber);
            Log.e(Tag,form.gps);
            Log.e(Tag,form.destination);

            Log.e(Tag,""+form.startKm);
            Log.e(Tag,""+form.endKm);
            Log.e(Tag,""+form.totalKm);

            Log.e(Tag,form.headLights);
            Log.e(Tag,form.tailLights);
            Log.e(Tag,form.brakeLights);
            Log.e(Tag,form.highBeam);
            Log.e(Tag,form.signalLights);

            Log.e(Tag,form.backupLights);
            Log.e(Tag,form.parkingBrake);
            Log.e(Tag,form.wheelTireTread);
            Log.e(Tag,form.mirrorWindow);
            Log.e(Tag,form.engineHoses);

            Log.e(Tag,""+form.fluidLevels);
            Log.e(Tag,""+form.belts);
            Log.e(Tag,""+form.battery);
            Log.e(Tag,""+form.exhaust);
            Log.e(Tag,""+form.instrumentGauges);
            Log.e(Tag,""+form.alarm);
            Log.e(Tag,""+form.horn);
            Log.e(Tag,""+form.heatDefrost);
            Log.e(Tag,""+form.fireExtinguisher);
            Log.e(Tag,""+form.frontierSafetyManual);
            Log.e(Tag,""+form.MSDS);
            Log.e(Tag,""+form.OHSA);
            Log.e(Tag,""+form.firstAidKit);
            Log.e(Tag,""+form.eyeWash);
            Log.e(Tag,""+form.remarks);
            Log.e(Tag,""+form.date);
            Log.e(Tag,""+form.isSubmitted);
        }

        Intent i = new Intent(getActivity(), FormActivty.class);
        i.putExtra("INDEX", position);
        getActivity().startActivity(i);

    }
}