package ca.utilitylocator.vehicleinspectionform.interfaces;

public interface TimePickerCallBack{
    void setTime(int hour, int minute);
}