package ca.utilitylocator.vehicleinspectionform.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

import ca.utilitylocator.vehicleinspectionform.interfaces.DatePickerCallback;

/**
 * Created by alvaregd on 06/07/15.
 */
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    DatePickerCallback listener;

    public void injectListener(DatePickerCallback listener){
        this.listener = listener;
    }

    public static DatePickerFragment newInstance(DatePickerCallback listener){
        final DatePickerFragment frag = new DatePickerFragment();
        frag.injectListener(listener);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        listener.setDate(monthOfYear,dayOfMonth,year);
    }
}
