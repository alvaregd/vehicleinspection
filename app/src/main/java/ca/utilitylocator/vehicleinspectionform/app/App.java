package ca.utilitylocator.vehicleinspectionform.app;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import ca.utilitylocator.vehicleinspectionform.data.FormCollection;

public class App extends Application {

    private final static String Tag= App.class.getName();
    private final static boolean d_save = true;
    private final static boolean d_load = true;


    private final static String FORM_PREFS = "FORMPREFS";
    private final static String FORM_KEY = "FORMKEY";

    SharedPreferences prefs;
    Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = getSharedPreferences(FORM_PREFS, Activity.MODE_PRIVATE);
        gson = new Gson();
    }

    public FormCollection getFormCollection(){

        if(d_load)Log.e(Tag,"getFormCollection()");

        FormCollection collection;

        String data = prefs.getString(FORM_KEY,"");
        if(data.isEmpty() || data.equals("")){
            collection = new FormCollection();
        }else{
            collection = gson.fromJson(data, FormCollection.class);
        }

        return collection;
    }

    public  void saveFormCollection(FormCollection collection){
        if(d_save) Log.e(Tag,"saveFormCollection()");

        if(collection != null){
            String data = gson.toJson(collection);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(FORM_KEY,data);
            edit.commit();
        }
    }
}
