package ca.utilitylocator.vehicleinspectionform.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import ca.utilitylocator.vehicleinspectionform.R;

public class FormListAdapter extends ArrayAdapter<String> {

    private Context context;

    public FormListAdapter(Context context) {
        super(context, R.layout.entry_form_list);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.entry_form_list,parent, false);
        }

        TextView tv = (TextView)convertView.findViewById(R.id.tv_creationTime);
        tv.setText(getItem(position));

        return convertView;
    }

}
