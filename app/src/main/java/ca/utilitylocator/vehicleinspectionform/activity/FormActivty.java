package ca.utilitylocator.vehicleinspectionform.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.adapters.FormAdapter;
import ca.utilitylocator.vehicleinspectionform.app.App;
import ca.utilitylocator.vehicleinspectionform.data.Form;
import ca.utilitylocator.vehicleinspectionform.data.FormCollection;
import ca.utilitylocator.vehicleinspectionform.views.TabDotIndicator;

public class FormActivty extends AppCompatActivity {

    FormAdapter mFormAdapter;
    ViewPager mViewPager;
    TabDotIndicator indicator;

    Form form;
    int selectedFormIndex = -1;

    public interface NextPage{
        void onNextPageClicked();
    }

    NextPage mNextPage = new NextPage() {
        @Override
        public void onNextPageClicked() {
            if(mViewPager.getCurrentItem() < 4){
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1,true);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_activty);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedFormIndex = extras.getInt("INDEX",-1);
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mFormAdapter = new FormAdapter(getSupportFragmentManager(), mNextPage);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mFormAdapter);

        indicator = (TabDotIndicator)findViewById(R.id.tab_dot_indicator);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                indicator.setguide(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#2f805b")));
        actionBar.setTitle( Html.fromHtml("<font color='#FFFFFF'>" + "Inspection Form" + "</font>"));

        if(selectedFormIndex != -1){
            mViewPager.setCurrentItem(4,true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveFormToStorage(form);
        Toast.makeText(this,"Saved Form",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();



    }

    @Override
    protected void onStart() {
        super.onStart();
//        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.rgb(47, 128, 91)));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_form_activty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Form getForm(){

        if(selectedFormIndex == -1){
            if(form == null){
                form = new Form();
            }
        }else{
            if(form == null){
                FormCollection collection = ((App) getApplication()).getFormCollection();
                form = collection.forms.get(selectedFormIndex);
            }
//            mViewPager.setCurrentItem(4,true);
        }
        return form;
    }

    public void saveFormToStorage(Form form){

        FormCollection collection = ((App) getApplication()).getFormCollection();
        if(selectedFormIndex == -1){
            collection.forms.add(form);
            selectedFormIndex= collection.forms.size() -1;
        }else{
            collection.forms.set(selectedFormIndex,form);
        }
        ((App) getApplication()).saveFormCollection(collection);
    }
}
