package ca.utilitylocator.vehicleinspectionform.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.activity.FormActivty;
import ca.utilitylocator.vehicleinspectionform.data.Form;

public class RemarksFragment extends Fragment {

    private final static String Tag = RemarksFragment.class.getName();
    private final static boolean d_save = true;

    private static final String ARG_SECTION_NUMBER = "section_number";

    private Form form;
    private FormActivty.NextPage mListener;

    EditText remarks;

    public static RemarksFragment newInstance(int sectionNumber, FormActivty.NextPage listener) {
        RemarksFragment fragment = new RemarksFragment();
        fragment.mListener = listener;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public RemarksFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_remarks, container, false);


        form = ((FormActivty)getActivity()).getForm();
        remarks = (EditText)rootView.findViewById(R.id.et_remarks);

        remarks.setText(form.remarks);

        ((Button)rootView.findViewById(R.id.b_next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFragment();
                mListener.onNextPageClicked();
            }
        });
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        saveFragment();
    }

    public void saveFragment(){
        if(d_save)Log.e(Tag,"saving remarks");
        form.remarks = remarks.getText().toString();
    }
}
