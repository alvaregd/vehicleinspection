package ca.utilitylocator.vehicleinspectionform.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.activity.FormActivty;
import ca.utilitylocator.vehicleinspectionform.adapters.CheckListAdapter;
import ca.utilitylocator.vehicleinspectionform.data.CheckListItem;
import ca.utilitylocator.vehicleinspectionform.data.Form;


public class CheckListFragment extends ListFragment {

    private final static String Tag= CheckListFragment.class.getName();
    private final static boolean d_save= true;



    private static final String ARG_SECTION_NUMBER = "section_number";

    private View rootView;
    private Form form;
    private CheckListAdapter mAdapter;

    private FormActivty.NextPage mListener;

    public static CheckListFragment newInstance(int sectionNumber, FormActivty.NextPage listener) {
        CheckListFragment fragment = new CheckListFragment();
        fragment.mListener = listener;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public CheckListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_check_list, container, false);
        }
        form = ((FormActivty)getActivity()).getForm();


        mAdapter = new CheckListAdapter(getActivity());
        mAdapter.add(new CheckListItem("Head Lights",form.headLights ));
        mAdapter.add(new CheckListItem("Tail Lights",form.tailLights));
        mAdapter.add(new CheckListItem("Brake Lights",form.brakeLights));
        mAdapter.add(new CheckListItem("High Beams",form.highBeam));
        mAdapter.add(new CheckListItem("Signal Lights",form.signalLights));
        mAdapter.add(new CheckListItem("Backup Lights",form.backupLights));
        mAdapter.add(new CheckListItem("Parking Brake",form.parkingBrake));
        mAdapter.add(new CheckListItem("Wheels - Tire & Tread",form.wheelTireTread));
        mAdapter.add(new CheckListItem("Mirrors & Windows",form.mirrorWindow));
        mAdapter.add(new CheckListItem("Engine Hoses",form.engineHoses));
        mAdapter.add(new CheckListItem("Fluid Levels",form.fluidLevels));
        mAdapter.add(new CheckListItem("Belts",form.belts));
        mAdapter.add(new CheckListItem("Battery",form.battery));
        mAdapter.add(new CheckListItem("Exhaust",form.exhaust));
        mAdapter.add(new CheckListItem("Instrument Gauges",form.instrumentGauges));
        mAdapter.add(new CheckListItem("Alarm",form.alarm));
        mAdapter.add(new CheckListItem("Horn",form.horn));
        mAdapter.add(new CheckListItem("Heat & Defrost",form.heatDefrost));
        mAdapter.add(new CheckListItem("Fire Extinguisher",form.fireExtinguisher));
        mAdapter.add(new CheckListItem("Frontier Safety Manual",form.frontierSafetyManual));
        mAdapter.add(new CheckListItem("MSDS",form.MSDS));
        mAdapter.add(new CheckListItem("OHSA",form.OHSA));
        mAdapter.add(new CheckListItem("First Aid Kit",form.firstAidKit));
        mAdapter.add(new CheckListItem("Eyewash",form.eyeWash));

        setListAdapter(mAdapter);

        ((Button)rootView.findViewById(R.id.b_next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFragment();
                mListener.onNextPageClicked();
            }
        });
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        saveFragment();
    }

    public void saveFragment(){

        if(d_save) Log.e(Tag, "saving Check list");

        form.headLights = mAdapter.getItem(0).status ;
        form.tailLights = mAdapter.getItem(1).status ;
        form.brakeLights = mAdapter.getItem(2).status;
        form.highBeam = mAdapter.getItem(3).status  ;
        form.signalLights = mAdapter.getItem(4).status;
        form.backupLights = mAdapter.getItem(5).status;
        form.parkingBrake = mAdapter.getItem(6).status ;
        form.wheelTireTread = mAdapter.getItem(7).status;
        form.mirrorWindow = mAdapter.getItem(8).status ;
        form.engineHoses = mAdapter.getItem(9).status ;
        form.fluidLevels = mAdapter.getItem(10).status ;
        form.belts = mAdapter.getItem(11).status;
        form.battery = mAdapter.getItem(12).status;
        form.exhaust = mAdapter.getItem(13).status;
        form.instrumentGauges = mAdapter.getItem(14).status;
        form.alarm = mAdapter.getItem(15).status;
        form.horn = mAdapter.getItem(16).status ;
        form.heatDefrost = mAdapter.getItem(17).status;
        form.fireExtinguisher = mAdapter.getItem(18).status;
        form.frontierSafetyManual = mAdapter.getItem(19).status;
        form.MSDS = mAdapter.getItem(20).status ;
        form.OHSA = mAdapter.getItem(21).status ;
        form.firstAidKit = mAdapter.getItem(22).status ;
        form.eyeWash = mAdapter.getItem(23).status ;

    }
}
