package ca.utilitylocator.vehicleinspectionform.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ca.utilitylocator.vehicleinspectionform.R;
import ca.utilitylocator.vehicleinspectionform.activity.FormActivty;
import ca.utilitylocator.vehicleinspectionform.data.Form;

public class InfoOneFragment extends Fragment {
    private final static String Tag  = InfoOneFragment.class.getName();
    private final static boolean d_save = true;

    private static final String ARG_SECTION_NUMBER = "section_number";

    private View rootView;

    private Form form;

    private EditText name, license, kit, departure;

    private FormActivty.NextPage mListener;

    public static InfoOneFragment newInstance(int sectionNumber, FormActivty.NextPage listener) {
        InfoOneFragment fragment = new InfoOneFragment();
        fragment.mListener = listener;
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public InfoOneFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView == null){
            rootView = inflater.inflate(R.layout.fragment_trip_info_1, container, false);
        }

        form = ((FormActivty)getActivity()).getForm();

        name = (EditText)rootView.findViewById(R.id.et_name);
        license = (EditText)rootView.findViewById(R.id.et_license);
        kit = (EditText)rootView.findViewById(R.id.et_kit);
        departure = (EditText)rootView.findViewById(R.id.et_departure_point);

        name.setText(form.driver);
        license.setText(form.plate);
        kit.setText(form.kit);
        departure.setText(form.departure);

        ((Button)rootView.findViewById(R.id.b_next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveFragment();
                mListener.onNextPageClicked();
            }
        });
        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();

        saveFragment();
    }

    public void saveFragment(){
        if(d_save) Log.e(Tag,"save One");
        form.driver = name.getText().toString();
        form.plate = license.getText().toString();
        form.kit = kit.getText().toString();
        form.departure = departure.getText().toString();
    }
}
